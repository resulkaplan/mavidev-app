import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotesService} from '../notes.service';
import {Notes} from '../../shared/models/Notes';
import {NotificationManager} from '../../util/notification-manager';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  createNote: FormGroup;
  allNotes: Notes = {};
  constructor( private formBuilder: FormBuilder,
               private noteService: NotesService,
               private notificationService: NotificationManager) {
  }

  ngOnInit() {
    this.createNote = this.formBuilder.group({
      note: [null, Validators.required],
    });
  }
  saveNote() {
    this.noteService.createNotes(this.allNotes).subscribe(data => {
      if (this.allNotes.note.length > 0) {
        this.notificationService.showSuccessNotification('Succesful');
      } else {
        this.notificationService.showErrorNotification('Failure');
      }
    });
  }
}
