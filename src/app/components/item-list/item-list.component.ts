import { Component, OnInit } from '@angular/core';
import {Notes} from '../../shared/models/Notes';
import {NotesService} from '../notes.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  noteValue: Notes[] = [];
  constructor(public noteService: NotesService) { }

  ngOnInit() {
    this.getAllNotes();
  }

  getAllNotes() {
    this.noteService.getNotes().subscribe(result => {
      this.noteValue = result.response;
    });
  }

}
