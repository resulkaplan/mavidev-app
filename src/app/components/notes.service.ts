import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Notes} from '../shared/models/Notes';
import {GenericResponse} from '../shared/models/GenericResponse';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  ROOT_URL = 'http://localhost:8070';
  constructor(private http: HttpClient) { }

  createNotes(note: Notes) {
    const url = `${this.ROOT_URL}/saveNote`;
    return this.http.post<GenericResponse<Notes>>(url, note);
  }


  getNotes(): Observable<GenericResponse<Notes []>> {
    const url = `${this.ROOT_URL}/allNotes`;
    return this.http.get<GenericResponse<Notes[]>>(url);
  }
}
