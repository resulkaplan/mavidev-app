import {BaseResponse} from './BaseResponse';

export interface GenericResponse<T> extends BaseResponse {
  response?: T;
}
