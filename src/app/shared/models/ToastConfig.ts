export interface ToastConfig {
  progressBar: any;
  timeOut: number;
  toastClass: string;
  positionClass: string;
}
