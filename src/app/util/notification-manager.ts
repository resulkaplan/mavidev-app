import {Injectable} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ToastConfig} from '../shared/models/ToastConfig';


declare const $: any;

/**
 * An utility class to manage create-notification
 */
@Injectable()
export class NotificationManager {

  constructor(private toastrService: ToastrManager) {
  }

  showInfoNotification(message: string) {
    const options: ToastConfig = {progressBar: true, timeOut: 3000, toastClass: 'yellow', positionClass: 'toast-bottom-right'};
    this.toastrService.infoToastr(message, '', options);
  }

  showSuccessNotification(message: string) {
    const options: ToastConfig = {progressBar: true, timeOut: 3000, toastClass: 'green', positionClass: 'toast-bottom-right'};
    this.toastrService.successToastr(message, '', options);
  }

  showWarningNotification(message: string) {
    const options: ToastConfig = {progressBar: true, timeOut: 3000, toastClass: 'pink', positionClass: 'toast-bottom-right'};
    this.toastrService.warningToastr(message, '', options);
  }

  showErrorNotification(message: string) {
    const options: ToastConfig = {progressBar: true, timeOut: 3000, toastClass: 'red', positionClass: 'toast-bottom-right'};
    this.toastrService.errorToastr(message, '', options);
  }


}
